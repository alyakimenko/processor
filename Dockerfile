# syntax=docker/dockerfile:1
# build stage
FROM golang:1.17.7-alpine3.15 as builder

# set workdir
WORKDIR /src/processor

# install dependencies
COPY go.mod go.sum ./
RUN go mod download

# copy source code
COPY . .

# build a binary
RUN CGO_ENABLED=0 go build -o bin/processor cmd/processor/main.go

# final image
FROM alpine:3.15

# update the apk lists, install ca-certificates and update ca-certificates
RUN apk update && apk add ca-certificates && update-ca-certificates

# set workdir
WORKDIR /opt/processor

# copy the binary from the builder stage
COPY --from=builder /src/processor/bin/processor processor

# expose port
EXPOSE 8080/tcp

# set an executable
CMD [ "/opt/processor/processor" ]