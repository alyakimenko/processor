package v1

import (
	"encoding/json"
	"net/http"
	"strconv"
)

// ErrorResponse is a struct wrapper around http error.
type ErrorResponse struct {
	Status string `json:"status"`
	Detail string `json:"detail"`
}

// apiError is a convenient way to throw an error from http handler function.
func apiError(rw http.ResponseWriter, detail string, status int) error {
	doc := ErrorResponse{
		Status: strconv.Itoa(status),
		Detail: detail,
	}

	data, err := json.Marshal(doc)
	if err != nil {
		panic(err)
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(status)

	_, err = rw.Write(data)
	if err != nil {
		return err
	}

	return nil
}
