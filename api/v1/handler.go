// Package v1 implements the v1 of the processor's REST API.
package v1

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/alyakimenko/processor/models"
)

// ProcessService describes a process service interface that is required for the Handler.
type ProcessService interface {
	Process(ctx context.Context, input models.ProcessRequestInput) error
}

// Handler is an HTTP handler for v1 routes.
type Handler struct {
	mux *http.ServeMux

	service ProcessService
	logger  *logrus.Logger
}

// HandlerParams is an incoming params for the NewHandler function.
type HandlerParams struct {
	Service ProcessService
	Logger  *logrus.Logger
}

// NewHandler returns net http.Handler.
func NewHandler(params HandlerParams) *Handler {
	mux := http.NewServeMux()

	handler := &Handler{
		mux:     mux,
		service: params.Service,
		logger:  params.Logger,
	}

	handler.initRoutes()

	return handler
}

// ServeHTTP satisfies the http.Handler interface.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.mux.ServeHTTP(w, r)
}

// initRoutes initializes all routes for the Handler.
func (h *Handler) initRoutes() {
	h.mux.HandleFunc("/v1/process", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusMethodNotAllowed)

			return
		}

		h.process(w, r)
	})
}

// ProcessInput is a request model for the POST /v1/process endpoint.
type ProcessInputRequest struct {
	RequestID   string `json:"request_id"`
	RequestType string `json:"type"`
}

// process is an HTTP handler func for POST /v1/process endpoint.
func (h *Handler) process(w http.ResponseWriter, r *http.Request) {
	var req ProcessInputRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		h.logger.Errorf("request error: %s", err.Error())
		_ = apiError(w, err.Error(), http.StatusBadRequest)

		return
	}

	err := h.service.Process(r.Context(), models.ProcessRequestInput{
		RequestID:   req.RequestID,
		RequestType: models.RequestType(req.RequestType),
	})
	if err != nil {
		h.logger.Errorf("process error: %s", err.Error())
		_ = apiError(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusOK)
}
