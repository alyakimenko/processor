package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/alyakimenko/processor/api"
	v1 "gitlab.com/alyakimenko/processor/api/v1"
	"gitlab.com/alyakimenko/processor/config"
	"gitlab.com/alyakimenko/processor/db/psql"
	"gitlab.com/alyakimenko/processor/logger"
	"gitlab.com/alyakimenko/processor/service"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/lib/pq"
)

func main() {
	// init global config
	config := config.NewConfig()

	// init logger
	logger, err := logger.NewLogrusLogger(config.Logger)
	if err != nil {
		panic(err)
	}

	// init database
	psqlDatabase, err := psql.NewDatabase(psql.DatabaseParams{
		Config: config.Database,
		Logger: logger,
	})
	if err != nil {
		panic(err)
	}

	// create process service with the database
	processService := service.NewProcessService(service.ProcessServiceParams{
		Database: psqlDatabase,
		Logger:   logger,
	})

	// init v1 HTTP handler
	handler := v1.NewHandler(v1.HandlerParams{
		Service: processService,
		Logger:  logger,
	})

	// create HTTP server with the initialized v1 handler
	server := api.NewServer(api.ServerParams{
		Config:  config.Server,
		Handler: handler,
	})

	// start the server
	go func() {
		if err := server.Start(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Errorf("server caught an error: %s\n", err.Error())
		}
	}()

	// graceful shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)

	<-quit

	if err := server.Shutdown(context.Background()); err != nil {
		logger.Errorf("failed to stop server: %s\n", err.Error())
	}
}
