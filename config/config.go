package config

import (
	"net"
	"net/url"
	"time"
)

// Config holds all configurable values for the processor service.
type Config struct {
	Server   ServerConfig
	Database DatabaseConfig
	Logger   LoggerConfig
}

// ServerConfig holds server related configurable values.
type ServerConfig struct {
	Port         string
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
	IdleTimeout  time.Duration
}

// Addr returns server's address in the format host:port.
func (sc ServerConfig) Addr() string {
	return net.JoinHostPort("", sc.Port)
}

// DatabaseConfig holds database related configurable values.
type DatabaseConfig struct {
	Host     string
	Port     string
	Name     string
	User     string
	Password string

	MaxOpenConns    int
	ConnMaxLifetime time.Duration
	Scheme          string
	SSLMode         string
}

// ConnectionURL constructs database connection URL based on a config values.
func (dc DatabaseConfig) ConnectionURL() string {
	u := &url.URL{
		Scheme: dc.Scheme,
		User:   url.UserPassword(dc.User, dc.Password),
		Host:   net.JoinHostPort(dc.Host, dc.Port),
		Path:   dc.Name,
		RawQuery: url.Values{
			"sslmode": []string{dc.SSLMode},
		}.Encode(),
	}

	return u.String()
}

// LoggerConfig holds logger related configurable values.
type LoggerConfig struct {
	Level string
}

// NewConfig initiates new Config with parsed environment variables.
func NewConfig() Config {
	return Config{
		Server: ServerConfig{
			Port:         EnvString("SERVER_PORT", "8080"),
			ReadTimeout:  EnvDuration("SERVER_READ_TIMEOUT", 5*time.Second),
			WriteTimeout: EnvDuration("SERVER_WRITE_TIMEOUT", 15*time.Second),
			IdleTimeout:  EnvDuration("SERVER_IDLE_TIMEOUT", 5*time.Second),
		},
		Logger: LoggerConfig{
			Level: EnvString("LOGGER_LEVEL", "INFO"),
		},
		Database: DatabaseConfig{
			Host:            EnvString("DATABASE_HOST", "localhost"),
			Port:            EnvString("DATABASE_PORT", "5432"),
			Name:            EnvString("DATABASE_NAME", "processor"),
			User:            EnvString("DATABASE_USER", "postgres"),
			Password:        EnvString("DATABASE_PASSWORD", "postgres"),
			MaxOpenConns:    EnvInt("DATABASE_MAX_OPEN_CONNECTIONS", 5),
			ConnMaxLifetime: EnvDuration("DATABASE_CONNECTION_MAX_LIFETIME", 30*time.Minute),
			Scheme:          EnvString("DATABASE_SCHEME", "postgres"),
			SSLMode:         EnvString("DATABASE_SSL_MODE", "disable"),
		},
	}
}
