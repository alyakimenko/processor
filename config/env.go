package config

import (
	"os"
	"strconv"
	"time"
)

// EnvString parses string environment variable.
func EnvString(name, defaultValue string) string {
	value := os.Getenv(name)
	if value == "" {
		return defaultValue
	}

	return value
}

// EnvInt parses int  environment variable.
func EnvInt(name string, defaultValue int) int {
	value := os.Getenv(name)
	if value == "" {
		return defaultValue
	}

	// ignore error and just return default value, for the sake of simplicity
	// in real world it should return an error as well
	parsedValue, err := strconv.Atoi(value)
	if err != nil {
		return defaultValue
	}

	return parsedValue
}

// EnvDuration parses duration environment variable.
func EnvDuration(name string, defaultValue time.Duration) time.Duration {
	value := os.Getenv(name)
	if value == "" {
		return defaultValue
	}

	// ignore error and just return default value, for the sake of simplicity
	// in real world it should return an error as well
	parsedValue, err := time.ParseDuration(value)
	if err != nil {
		return defaultValue
	}

	return parsedValue
}
