package db

import "errors"

// ErrNotFound occurs when desired element is not found in a database.
var ErrNotFound = errors.New("not found")
