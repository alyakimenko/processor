package memory

import (
	"context"
	"sync"

	"gitlab.com/alyakimenko/processor/db"
	"gitlab.com/alyakimenko/processor/models"
)

// Database represents repository level with memory implementation.
// This implementation is for tests only.
type Database struct {
	sync.RWMutex

	data map[string]*models.Process
}

// NewDatabase creates new instance of the Database.
func NewDatabase() *Database {
	return &Database{
		data: make(map[string]*models.Process),
	}
}

// CreateProcess creates new process in memory.
func (d *Database) CreateProcess(ctx context.Context, input models.ProcessCreateInput) error {
	d.Lock()
	defer d.Unlock()

	d.data[input.RequestID] = &models.Process{
		RequestID: input.RequestID,
		Step:      input.Step,
		Status:    input.Status,
	}

	return nil
}

// UpdateProcess updates process that is stored in memory.
func (d *Database) UpdateProcess(ctx context.Context, input models.ProcessUpdateInput) error {
	d.Lock()
	defer d.Unlock()

	process, ok := d.data[input.RequestID]
	if !ok {
		return db.ErrNotFound
	}

	if process.Status == models.ProcessStatusRunning {
		process.Step = input.Step
		process.Status = input.Status
	}

	d.data[input.RequestID] = process

	return nil
}

// GetProcess returns a process by the provided requestID.
func (d *Database) GetProcess(ctx context.Context, requestID string) (*models.Process, error) {
	d.RLock()
	defer d.RUnlock()

	process, ok := d.data[requestID]
	if !ok {
		return nil, db.ErrNotFound
	}

	return process, nil
}
