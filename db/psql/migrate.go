package psql

import (
	"embed"

	gomigrate "github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/source/iofs"
)

//go:embed migrations
var migrations embed.FS

// migrate migrates a database.
func migrate(connectionURL string) error {
	d, err := iofs.New(migrations, "migrations")
	if err != nil {
		return err
	}

	m, err := gomigrate.NewWithSourceInstance("iofs", d, connectionURL)
	if err != nil {
		return err
	}

	err = m.Up()
	if err != nil {
		return err
	}

	return nil
}
