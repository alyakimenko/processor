create table if not exists processes(
    request_id  text    primary key,
    step        int     not null,
    status      text    not null
);