package psql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	gomigrate "github.com/golang-migrate/migrate/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/alyakimenko/processor/config"
	"gitlab.com/alyakimenko/processor/db"
	"gitlab.com/alyakimenko/processor/models"
)

// Database represents repository level with PostgreSQL implementation.
type Database struct {
	db     *sql.DB
	logger *logrus.Logger
}

// DatabaseParams is an incoming params for the NewDatabase function.
type DatabaseParams struct {
	Config config.DatabaseConfig
	Logger *logrus.Logger
}

// NewDatabase creates new instance of the Database.
func NewDatabase(params DatabaseParams) (*Database, error) {
	db, err := sql.Open("postgres", params.Config.ConnectionURL())
	if err != nil {
		return nil, fmt.Errorf("failed to connect to db: %w", err)
	}

	db.SetMaxOpenConns(params.Config.MaxOpenConns)
	db.SetConnMaxLifetime(params.Config.ConnMaxLifetime)

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("failed to ping db: %w", err)
	}

	if err := migrate(params.Config.ConnectionURL()); err != nil {
		if !errors.Is(err, gomigrate.ErrNoChange) {
			return nil, fmt.Errorf("failed to migrate db: %w", err)
		}

		params.Logger.Infof("migrations have no changes")
	}

	return &Database{
		db:     db,
		logger: params.Logger,
	}, nil
}

// SaveProcess saves process into a database.
func (d *Database) CreateProcess(ctx context.Context, input models.ProcessCreateInput) error {
	_, err := d.db.Exec("insert into processes (request_id, step, status) values ($1, $2, $3)",
		input.RequestID, input.Step, input.Status)
	if err != nil {
		return fmt.Errorf("failed to insert process: %w", err)
	}

	return nil
}

// UpdateProcess updates a process within a database.
func (d *Database) UpdateProcess(ctx context.Context, input models.ProcessUpdateInput) error {
	tx, err := d.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to begin transaction: %w", err)
	}

	_, err = tx.Exec(
		"update processes set step = $1, status = $2 where request_id = $3 and status = $4",
		input.Step, input.Status, input.RequestID, models.ProcessStatusRunning,
	)
	if err != nil {
		_ = tx.Rollback()

		return fmt.Errorf("failed to insert process: %w", err)
	}

	err = tx.Commit()
	if err != nil {
		_ = tx.Rollback()

		return fmt.Errorf("failed to commit transaction: %w", err)
	}

	return nil
}

// GetProcess returns process by the provided requestID.
func (d *Database) GetProcess(ctx context.Context, requestID string) (*models.Process, error) {
	row := d.db.QueryRowContext(ctx,
		"select request_id, step, status from processes where request_id = $1", requestID,
	)

	var process models.Process
	err := row.Scan(&process.RequestID, &process.Step, &process.Status)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, db.ErrNotFound
		}

		return nil, err
	}

	return &process, nil
}
