package logger

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/alyakimenko/processor/config"
)

// NewLogrusLogger initizalies new *logrus.Logger.
func NewLogrusLogger(config config.LoggerConfig) (*logrus.Logger, error) {
	logger := logrus.New()

	level, err := logrus.ParseLevel(config.Level)
	if err != nil {
		return nil, fmt.Errorf("failed to parse log level: %w", err)
	}

	logger.SetLevel(level)

	return logger, nil
}
