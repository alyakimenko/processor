package models

// ProcessStatus represents a process status.
type ProcessStatus string

const (
	ProcessStatusRunning = "running"
	// ProcessStatusStoped means that process was stoped gracefully.
	ProcessStatusStoped = "stoped"
	// ProcessStatusFinished means that process was successfully finished.
	ProcessStatusFinished = "finished"
)

// RequestType represents a process request type.
type RequestType string

const (
	// RequestTypeStart is a start request type.
	RequestTypeStart RequestType = "start"
	// RequestTypeStart is a stop request type.
	RequestTypeStop RequestType = "stop"
)

// Process represents core Process model.
type Process struct {
	RequestID string
	Step      int
	Status    ProcessStatus
}

// ProcessRequestInput represents request model for processing.
type ProcessRequestInput struct {
	RequestID   string
	RequestType RequestType
}

// ProcessCreateInput represents model for process creation.
type ProcessCreateInput struct {
	RequestID string
	Step      int
	Status    ProcessStatus
}

// ProcessUpdateInput represents model for process modification.
type ProcessUpdateInput struct {
	RequestID string
	Step      int
	Status    ProcessStatus
}
