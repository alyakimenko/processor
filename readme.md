### Running

In order to run the service alongside with PostgreSQL database you can just execute the command:

`docker-compose up --build`

The service is reachable at port `8080` by default.

### API

The app has only one endpoint.

`POST /v1/process`

Example request body:

```json
{
    "request_id": "74",
    "type": "start"
}
```

### Tests

See https://gitlab.com/alyakimenko/processor/-/blob/master/service/process_test.go.
