package service

import "errors"

var (
	// ErrProcessIsAlreadyExists occurs when user is trying to start a process that is already exists.
	ErrProcessIsAlreadyExists = errors.New("process is already exists")
	// ErrTooLateToStopProcess occurs when user is trying to stop process that has step greater than 5.
	ErrTooLateToStopProcess = errors.New("cannot stop process, it's too late")
	// ErrProcessDoesntExists occurs when user is trying to start process that doesn't exists yet.
	ErrProcessDoesntExists = errors.New("process doesn't exists")
	// ErrProcessIsAlreadyStoped occurs when user is trying to stop already stoped process.
	ErrProcessIsAlreadyStoped = errors.New("process is already stoped")
)
