package service

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/alyakimenko/processor/db"
	"gitlab.com/alyakimenko/processor/models"
)

// maxStepToClose is a maximum allowed step value when closing process is acceptable.
const maxStepToClose = 5

// ProcessDatabase describes an interface for process database that is required for the ProcessService.
type ProcessDatabase interface {
	CreateProcess(ctx context.Context, input models.ProcessCreateInput) error
	UpdateProcess(ctx context.Context, input models.ProcessUpdateInput) error
	GetProcess(ctx context.Context, requestID string) (*models.Process, error)
}

// ProcessService is a sample process service.
type ProcessService struct {
	database ProcessDatabase
	logger   *logrus.Logger
}

// ProcessServiceParams is an incoming params for the NewProcessService function.
type ProcessServiceParams struct {
	Database ProcessDatabase
	Logger   *logrus.Logger
}

// NewProcessService creates new instance of the ProcessService.
func NewProcessService(params ProcessServiceParams) *ProcessService {
	return &ProcessService{
		database: params.Database,
		logger:   params.Logger,
	}
}

// Process processes all required jobs.
// nolint:cyclop,gocognit
func (s *ProcessService) Process(ctx context.Context, input models.ProcessRequestInput) (err error) {
	process, err := s.database.GetProcess(ctx, input.RequestID)
	if err != nil && !errors.Is(err, db.ErrNotFound) {
		return err
	}

	if process != nil && input.RequestType == models.RequestTypeStop {
		if process.Step > maxStepToClose && process.Status == models.ProcessStatusRunning {
			return ErrTooLateToStopProcess
		}

		if process.Status == models.ProcessStatusStoped || process.Status == models.ProcessStatusFinished {
			return ErrProcessIsAlreadyStoped
		}

		if err := s.stopProcess(ctx, process.Step, input.RequestID); err != nil {
			return fmt.Errorf("cannot stop process: %w", err)
		}

		return nil
	}

	if process == nil && input.RequestType != models.RequestTypeStart {
		return ErrProcessDoesntExists
	}

	if process != nil && input.RequestType == models.RequestTypeStart {
		return ErrProcessIsAlreadyExists
	}

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 12*time.Second)
		defer cancel()

		if err := s.stepOne(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step one: %s", err)

			return
		}

		if err := s.stepTwo(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step two: %s", err)

			return
		}

		if err := s.stepThree(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step three: %s", err)

			return
		}

		if err := s.stepFour(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step four: %s", err)

			return
		}

		if err := s.stepFive(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step five: %s", err)

			return
		}

		if err := s.stepSix(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step six: %s", err)

			return
		}

		if err := s.stepSeven(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step seven: %s", err)

			return
		}

		if err := s.stepEight(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step eight: %s", err)

			return
		}

		if err := s.stepNine(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step nine: %s", err)

			return
		}

		if err := s.stepTen(ctx, input.RequestID); err != nil {
			s.logger.Errorf("failed to process step ten: %s", err)

			return
		}
	}()

	return nil
}

func (s *ProcessService) stepOne(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.CreateProcess(ctx, models.ProcessCreateInput{
		RequestID: requestID,
		Step:      1,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepTwo(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      2,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepThree(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      3,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepFour(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      4,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepFive(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      5,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepSix(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      6,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepSeven(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      7,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepEight(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      8,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepNine(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      9,
		Status:    models.ProcessStatusRunning,
	})
}

func (s *ProcessService) stepTen(ctx context.Context, requestID string) error {
	time.Sleep(time.Second)

	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      10,
		Status:    models.ProcessStatusFinished,
	})
}

func (s *ProcessService) stopProcess(ctx context.Context, step int, requestID string) error {
	return s.database.UpdateProcess(ctx, models.ProcessUpdateInput{
		RequestID: requestID,
		Step:      step,
		Status:    models.ProcessStatusStoped,
	})
}
