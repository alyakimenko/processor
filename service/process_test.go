package service

import (
	"context"
	"errors"
	"io"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/alyakimenko/processor/db/memory"
	"gitlab.com/alyakimenko/processor/models"
)

func TestProcess(t *testing.T) {
	t.Parallel()

	logger := logrus.New()
	// ignore logs
	logger.SetOutput(io.Discard)

	memoryDatabase := memory.NewDatabase()

	processService := NewProcessService(ProcessServiceParams{
		Database: memoryDatabase,
		Logger:   logger,
	})

	t.Run("multiple async start requests with different request_id", func(t *testing.T) {
		t.Parallel()

		err := processService.Process(context.Background(), models.ProcessRequestInput{
			RequestID:   "1",
			RequestType: models.RequestTypeStart,
		})
		if err != nil {
			t.Error("error is not expected")

			return
		}

		err = processService.Process(context.Background(), models.ProcessRequestInput{
			RequestID:   "2",
			RequestType: models.RequestTypeStart,
		})
		if err != nil {
			t.Error("error is not expected")

			return
		}
	})

	t.Run("multiple async start requests with one request_id", func(t *testing.T) {
		t.Parallel()

		err := processService.Process(context.Background(), models.ProcessRequestInput{
			RequestID:   "3",
			RequestType: models.RequestTypeStart,
		})
		if err != nil {
			t.Error("error is not expected")

			return
		}

		// sacrifice determinism for the sake of simplicity
		// possible solutions are:
		// 1. use mock with WaitGroup
		// 2. use channel within the Process method and wait on receiving
		time.Sleep(2 * time.Second)

		err = processService.Process(context.Background(), models.ProcessRequestInput{
			RequestID:   "3",
			RequestType: models.RequestTypeStart,
		})
		if err == nil {
			t.Error("error is expected")

			return
		}

		if !errors.Is(err, ErrProcessIsAlreadyExists) {
			t.Errorf("expected %s, got %s", ErrProcessIsAlreadyExists, err)

			return
		}
	})

	t.Run("successful stop before 5 step", func(t *testing.T) {
		t.Parallel()

		err := memoryDatabase.CreateProcess(context.Background(), models.ProcessCreateInput{
			RequestID: "4",
			Step:      4,
			Status:    models.ProcessStatusRunning,
		})
		if err != nil {
			t.Error("error is not expected")

			return
		}

		err = processService.Process(context.Background(), models.ProcessRequestInput{
			RequestID:   "4",
			RequestType: models.RequestTypeStop,
		})
		if err != nil {
			t.Error("error is not expected")

			return
		}
	})

	t.Run("unsuccessful stop after 5 step", func(t *testing.T) {
		t.Parallel()

		err := memoryDatabase.CreateProcess(context.Background(), models.ProcessCreateInput{
			RequestID: "5",
			Step:      6,
			Status:    models.ProcessStatusRunning,
		})
		if err != nil {
			t.Error("error is not expected")

			return
		}

		err = processService.Process(context.Background(), models.ProcessRequestInput{
			RequestID:   "5",
			RequestType: models.RequestTypeStop,
		})
		if err == nil {
			t.Error("error is expected")

			return
		}

		if !errors.Is(err, ErrTooLateToStopProcess) {
			t.Errorf("expected %s, got %s", ErrTooLateToStopProcess, err)

			return
		}
	})
}
